﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PortalInmobiliario.AccesoDatos;
using System.Data;

namespace PortalInmobiliario.Presentacion
{
    public partial class ProyectoDetalle : System.Web.UI.Page
    {
        ADProyecto AccesoDatosProyecto = new ADProyecto();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                int projectID = Int32.Parse(Request.QueryString["id"]);
                DataTable dt = AccesoDatosProyecto.extrae("sp_consultar_proyecto_x_id", "@id", projectID);
                fvProyecto.DataSource = dt;
                fvProyecto.DataBind();
                List<int> listaPisos = new List<int>();
                foreach (DataRow row in dt.Rows)
                {
                    for (int i = 1; i <= Int32.Parse(row["pisos"].ToString()); i++)
                    {
                        listaPisos.Add(i);
                    }
                }
                ddlPisos.DataSource = listaPisos;
                ddlPisos.DataBind();
                ddlPisos.Items.Insert(0, new ListItem("---", ""));
            }
        }

        protected void ddlPisos_SelectedIndexChanged(object sender, EventArgs e)
        {
            int projectID = Int32.Parse(Request.QueryString["id"]);

            try
            {
                dtlDepartamentos.DataSource = AccesoDatosProyecto.extrae("sp_consultar_departamentos_disponibles_x_piso", "@piso", Int32.Parse(ddlPisos.SelectedValue), "@proyecto_id", projectID);
                dtlDepartamentos.DataBind();
            } catch
            {

            }
        }
    }
}