﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PortalInmobiliario.AccesoDatos;
using System.Data;

namespace PortalInmobiliario.Presentacion
{
    public partial class CalcularCuotas : System.Web.UI.Page
    {
        ADDepartamento AccesoDatosDepartamento = new ADDepartamento();
        ADEntidadFinanciera AccesoDatosFinanciera = new ADEntidadFinanciera();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                int departmentID = Int32.Parse(Request.QueryString["departamento_id"]);
                DataTable dt = AccesoDatosDepartamento.extrae("sp_consultar_departamento_x_id", "@id", departmentID);
                
                foreach (DataRow row in dt.Rows)
                {
                    txbPrecio.Text = row["precio"].ToString();
                }

                ddlFinancieras.DataSource = AccesoDatosFinanciera.extrae("sp_consultar_entidades_financieras");
                ddlFinancieras.DataTextField = "nombre";
                ddlFinancieras.DataValueField = "id";
                ddlFinancieras.DataBind();
                ddlFinancieras.Items.Insert(0, new ListItem("---", ""));
            }
        }

        protected void btnCalcular_Click(object sender, EventArgs e)
        {
            try
            {
                double cuota, t, b, taza, monto, mesesPlazo;
                taza = double.Parse(txbInteres.Text);
                mesesPlazo = double.Parse(txbAños.Text) * 12;
                monto = double.Parse(txbFinanciamiento.Text);
                t = taza / 1200;
                b = Math.Pow((1 + t), mesesPlazo);
                cuota = t * monto * b / (b - 1);
                lblCuotas.Text = "Su Cuota Mensual es S/." + Math.Round(cuota, 2);
            } catch
            {

            }
        }

        protected void ddlFinancieras_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int financialID = Int32.Parse(ddlFinancieras.SelectedValue);
                double cuotaInicial, precioInmueble;
                DataTable dt = AccesoDatosFinanciera.extrae("sp_consultar_entidad_financiera_x_id", "@id", financialID);

                foreach (DataRow row in dt.Rows)
                {
                    txbCuotaInicial.Text = row["cuota_inicial"].ToString();
                    txbInteres.Text = row["tasa_interes"].ToString();
                    cuotaInicial = double.Parse(txbCuotaInicial.Text);
                    precioInmueble = double.Parse(txbPrecio.Text);
                    txbFinanciamiento.Text = (precioInmueble - cuotaInicial).ToString();
                }
            } catch
            {

            }
        }
    }
}