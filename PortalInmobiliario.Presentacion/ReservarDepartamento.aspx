﻿<%@ Page Title="Reserva de Departamento" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReservarDepartamento.aspx.cs" Inherits="PortalInmobiliario.Presentacion.ReservarDepartamento" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content"><div class="ic">More Website Templates @ TemplateMonster.com - June 02, 2014!</div>
        <div class="container">
            <div class="row">
                <div class="grid_7">
                    <h3>Reserva del Departamento</h3>
                    <form id="form" runat="server">
                        <br>
                        <asp:Label ID="Label1" runat="server" Text="DNI: "></asp:Label>
                        <asp:TextBox ID="txbDNI" runat="server"></asp:TextBox>
                        <br>
                        <br>
                        <asp:Label ID="Label2" runat="server" Text="Apellido Paterno: "></asp:Label>
                        <asp:TextBox ID="txbApellidoPaterno" runat="server"></asp:TextBox>
                        <br>
                        <br>
                        <asp:Label ID="Label3" runat="server" Text="Apellido Materno: "></asp:Label>
                        <asp:TextBox ID="txbApellidoMaterno" runat="server"></asp:TextBox>
                        <br>
                        <br>
                        <asp:Label ID="Label4" runat="server" Text="Nombres: "></asp:Label>
                        <asp:TextBox ID="txbNombres" runat="server"></asp:TextBox>
                        <br>
                        <br>
                        <asp:Label ID="Label5" runat="server" Text="Teléfono: "></asp:Label>
                        <asp:TextBox ID="txbTelefono" runat="server"></asp:TextBox>
                        <br>
                        <br>
                        <asp:Label ID="Label6" runat="server" Text="Correo Electrónico: "></asp:Label>
                        <asp:TextBox ID="txbCorreoElectronico" runat="server"></asp:TextBox>
                        <br>
                        <br>
                        <div>
                            <div class="clear"></div>
                            <div class="btns">
                                <asp:LinkButton ID="btnReservar" type="submit" class="btn" runat="server" OnClick="btnReservar_Click">Reservar</asp:LinkButton>
                            </div>
                        </div>
                        <br>
                        <br>
                        <br>
                        <asp:Label ID="lblRespuesta" runat="server" Text=""></asp:Label>
                    </form>
                </div>
                <div class="grid_4 preffix_1">
                    
                </div>
            </div>
        </div>
    </section>
    <script>
        document.getElementById("ProjectsOption").className = "current";
    </script>
</asp:Content>
