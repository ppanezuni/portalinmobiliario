﻿using System;
using System.Web.UI;
using PortalInmobiliario.AccesoDatos;

namespace PortalInmobiliario.Presentacion
{
    public partial class Proyectos : System.Web.UI.Page
    {
        ADProyecto AccesoDatosProyecto = new ADProyecto();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                dtlProyectos.DataSource = AccesoDatosProyecto.extrae("sp_consultar_proyectos_x_ultimos_departamentos_disponibles");
                dtlProyectos.DataBind();
            }
        }
    }
}