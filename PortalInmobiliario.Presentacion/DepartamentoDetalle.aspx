﻿<%@ Page Title="Detalle de Departamento" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DepartamentoDetalle.aspx.cs" Inherits="PortalInmobiliario.Presentacion.DepartamentoDetalle" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content"><div class="ic">More Website Templates @ TemplateMonster.com - June 02, 2014!</div>
        <div class="container">
            <div class="row">
                <form runat="server">
                    <asp:FormView ID="fvDepartamento" runat="server">
                        <ItemTemplate>
                            <div class="grid_7">
                                <h3><%# Eval("codigo") %></h3>
                                <div class="blog">
                                    <!-- <time datetime="2014-01-01"><span class="fa fa-calendar"></span>21 May
                                    </time> -->
                                    <div class="extra_wrapper">
                                        <div class="text1"><a href="#"></a></div>
                                    </div>
                                    <div class="clear"></div>
                                    <img src="/Content/static/img/page4_img1.jpg" alt="" class="img_inner fleft">
                                    <div class="extra_wrapper">
                                        Datos del Departamento: 
                                        <br>
                                        <br>
                                        <ul>
                                            <li>Precio: S/.<%# Eval("precio") %></li>
                                            <li>Área: <%# Eval("area_total") %> m2</li>
                                            <li>Se encuentra ubicado en el piso <%# Eval("piso_ubicado") %></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="grid_4 preffix_1">
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <a href="/ReservarDepartamento?id=<%# Eval("id") %>" class="btn">Reservar</a>
                                <a href="/CalcularCuotas?departamento_id=<%# Eval("id") %>" class="btn">Calcular cuotas</a>
                                <a href="/ContactarAgente?departamento_id=<%# Eval("id") %>" class="btn">Contactar agente</a>
                            </div>
                        </ItemTemplate>
                    </asp:FormView>
                </form>
            </div>
        </div>
    </section>
    <script>
        document.getElementById("ProjectsOption").className = "current";
    </script>
</asp:Content>
