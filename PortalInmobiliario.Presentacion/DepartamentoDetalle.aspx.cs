﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PortalInmobiliario.AccesoDatos;

namespace PortalInmobiliario.Presentacion
{
    public partial class DepartamentoDetalle : System.Web.UI.Page
    {
        ADDepartamento AccesoDatosDepartamento = new ADDepartamento();

        protected void Page_Load(object sender, EventArgs e)
        {
            int departmentID = Int32.Parse(Request.QueryString["id"]);
            fvDepartamento.DataSource = AccesoDatosDepartamento.extrae("sp_consultar_departamento_x_id", "@id", departmentID);
            fvDepartamento.DataBind();
        }
    }
}