﻿<%@ Page Title="Dashboard | Mantenimiento Departamentos" Language="C#" MasterPageFile="~/Dashboard.master" AutoEventWireup="true" CodeBehind="DptosXProyecto.aspx.cs" Inherits="PortalInmobiliario.Presentacion.Mantenimiento.DptosXProyecto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <h1>MANTENIMIENTO DE DEPARTAMENTOS POR PROYECTO INMOBILIARIO</h1>
    <br />
    <div class="form-group row col-sm-12">
        <div class="col-sm-2">
            <asp:Label ID="Label1" runat="server" Text="Proyecto: "></asp:Label>
        </div>
        <div class="col-sm-4">
            <asp:DropDownList ID="ddlProyectos" class="form-control" runat="server" required>
            </asp:DropDownList>
        </div>
    </div>
    <div class="form-group row col-sm-12">
        <div class="col-sm-2">
            <asp:Label ID="Label2" runat="server" Text="Código: "></asp:Label>
        </div>
        <div class="col-sm-10">
            <asp:TextBox ID="txbCodigo" class="form-control" runat="server" MaxLength="16" required></asp:TextBox>
        </div>
    </div>
    <div class="form-group row col-sm-12">
        <div class="col-sm-2">
            <asp:Label ID="Label3" runat="server" Text="Área Total: "></asp:Label>
        </div>
        <div class="col-sm-10">
            <asp:TextBox ID="txbAreaTotal" class="form-control" runat="server" MaxLength="16"></asp:TextBox>
        </div>
    </div>
    <div class="form-group row col-sm-12">
        <div class="col-sm-2">
            <asp:Label ID="Label4" runat="server" Text="Precio por metro cuadrado: "></asp:Label>
        </div>
        <div class="col-sm-10">
            <asp:TextBox ID="txbPrecioM2" class="form-control" runat="server" MaxLength="16" required></asp:TextBox>
        </div>
    </div>
    <div class="form-group row col-sm-12">
        <div class="col-sm-2">
            <asp:Label ID="Label5" runat="server" Text="Moneda: "></asp:Label>
        </div>
        <div class="col-sm-4">
            <asp:DropDownList ID="ddlMonedas" class="form-control" runat="server" required>
                <asp:ListItem>Dólares</asp:ListItem>
                <asp:ListItem>Soles</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div class="form-group row col-sm-12">
        <div class="col-sm-2">
            <asp:Label ID="Label6" runat="server" Text="Piso Ubicado: "></asp:Label>
        </div>
        <div class="col-sm-10">
            <asp:TextBox ID="txbPisoUbicado" type="number" min="1" class="form-control" runat="server" MaxLength="4" required></asp:TextBox>
        </div>
    </div>
    <br />
    <asp:Button ID="btnNuevo" type="reset" class="btn btn-default" runat="server" OnClick="btnNuevo_Click" Text="Nuevo" />
    <asp:Button ID="btnGuardar" type="submit" class="btn btn-success" runat="server" OnClick="btnGuardar_Click" Text="Guardar" />
    <br />
    <br />
    <asp:GridView class="table table-bordered bs-table" ID="dgvDepartamentos" runat="server">
    </asp:GridView>
</asp:Content>