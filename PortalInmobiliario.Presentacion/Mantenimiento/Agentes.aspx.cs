﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PortalInmobiliario.Entidades;
using PortalInmobiliario.AccesoDatos;

namespace PortalInmobiliario.Presentacion.Mantenimiento
{
    public partial class Agentes : System.Web.UI.Page
    {
        Agente agente_inmobiliario = new Agente();
        ADAgente acceso_datos_agente = new ADAgente();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                rellena_grid();
            }
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            txbDNI.Text = "";
            txbApellidoPaterno.Text = "";
            txbApellidoMaterno.Text = "";
            txbNombres.Text = "";
            txbTelefono.Text = "";
            txbCorreoElectronico.Text = "";
            txbDNI.Focus();
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            agente_inmobiliario.Dni = txbDNI.Text;
            agente_inmobiliario.ApellidoPaterno = txbApellidoPaterno.Text;
            agente_inmobiliario.ApellidoMaterno = txbApellidoMaterno.Text;
            agente_inmobiliario.Nombres = txbNombres.Text;
            agente_inmobiliario.Telefono = txbTelefono.Text;
            agente_inmobiliario.CorreoElectronico = txbCorreoElectronico.Text;
            acceso_datos_agente.crear("sp_insertar_agente", agente_inmobiliario);
            rellena_grid();
        }

        public void rellena_grid()
        {
            dgvAgentes.DataSource = acceso_datos_agente.extrae("sp_consultar_agentes");
            dgvAgentes.DataBind();
        }
    }
}