﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PortalInmobiliario.Entidades;
using PortalInmobiliario.AccesoDatos;

namespace PortalInmobiliario.Presentacion.Mantenimiento
{
    public partial class Clientes : System.Web.UI.Page
    {
        Cliente cliente_registrado = new Cliente();
        ADCliente acceso_datos_cliente = new ADCliente();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                rellena_grid();
            }
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            txbDNI.Text = "";
            txbApellidoPaterno.Text = "";
            txbApellidoMaterno.Text = "";
            txbNombres.Text = "";
            txbTelefono.Text = "";
            txbCorreoElectronico.Text = "";
            txbDNI.Focus();
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            cliente_registrado.Dni = txbDNI.Text;
            cliente_registrado.ApellidoPaterno = txbApellidoPaterno.Text;
            cliente_registrado.ApellidoMaterno = txbApellidoMaterno.Text;
            cliente_registrado.Nombres = txbNombres.Text;
            cliente_registrado.Telefono = txbTelefono.Text;
            cliente_registrado.CorreoElectronico = txbCorreoElectronico.Text;
            acceso_datos_cliente.crear("sp_insertar_cliente", cliente_registrado);
            rellena_grid();
        }

        public void rellena_grid()
        {
            dgvClientes.DataSource = acceso_datos_cliente.extrae("sp_consultar_clientes");
            dgvClientes.DataBind();
        }
    }
}