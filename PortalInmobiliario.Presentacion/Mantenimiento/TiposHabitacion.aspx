﻿<%@ Page Title="Dashboard | Mantenimiento Tipos" Language="C#" MasterPageFile="~/Dashboard.master" AutoEventWireup="true" CodeBehind="TiposHabitacion.aspx.cs" Inherits="PortalInmobiliario.Presentacion.Mantenimiento.TiposHabitacion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <h1>MANTENIMIENTO DE TIPOS DE HABITACIÓN</h1>
    <br />
    <div class="form-group row col-sm-12">
        <div class="col-sm-2">
            <asp:Label ID="Label1" runat="server" Text="Nombre: "></asp:Label>
        </div>
        <div class="col-sm-10">
            <asp:TextBox ID="txbNombre" class="form-control" runat="server" MaxLength="16" required></asp:TextBox>
        </div>
    </div>
    <br />
    <asp:Button ID="btnNuevo" type="reset" class="btn btn-default" runat="server" OnClick="btnNuevo_Click" Text="Nuevo" />
    <asp:Button ID="btnGuardar" type="submit" class="btn btn-success" runat="server" OnClick="btnGuardar_Click" Text="Guardar" />
    <br />
    <br />
    <asp:GridView class="table table-bordered bs-table" ID="dgvTiposHab" runat="server">
    </asp:GridView>
</asp:Content>