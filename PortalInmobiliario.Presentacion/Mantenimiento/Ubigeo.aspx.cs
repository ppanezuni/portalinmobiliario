﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PortalInmobiliario.Entidades;
using PortalInmobiliario.AccesoDatos;
using System.IO;
using System.Data;

namespace PortalInmobiliario.Presentacion.Mantenimiento
{
    public partial class Ubigeo : System.Web.UI.Page
    {
        UbigeoDepartamento departamento = new UbigeoDepartamento();
        ADUbigeoDepartamento acceso_datos_departamento = new ADUbigeoDepartamento();
        UbigeoProvincia provincia = new UbigeoProvincia();
        ADUbigeoProvincia acceso_datos_provincia = new ADUbigeoProvincia();
        UbigeoDistrito distrito = new UbigeoDistrito();
        ADUbigeoDistrito acceso_datos_distrito = new ADUbigeoDistrito();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAgregarDepartamentos_Click(object sender, EventArgs e)
        {
            string ruta_archivo_csv = MapPath("~/Content/static/files/equivalencia-ubigeos-oti-concytec.csv");

            using (var fs = File.OpenRead(ruta_archivo_csv))
            using (var reader = new StreamReader(fs))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');

                    departamento.Nombre = values[1].ToString();
                    provincia.Nombre = values[3].ToString();

                    if (!(acceso_datos_departamento.existe("sp_consultar_ubigeo_departamento_x_nombre", "@nombre", departamento.Nombre) || departamento.Nombre == string.Empty || departamento.Nombre == "desc_dep_inei"))
                    {
                        acceso_datos_departamento.crear("sp_insertar_ubigeo_departamento", departamento);
                    }
                }
            }
        }

        protected void btnAgregarProvincias_Click(object sender, EventArgs e)
        {
            string ruta_archivo_csv = MapPath("~/Content/static/files/equivalencia-ubigeos-oti-concytec.csv");

            using (var fs = File.OpenRead(ruta_archivo_csv))
            using (var reader = new StreamReader(fs))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');

                    departamento.Nombre = values[1].ToString();
                    provincia.Nombre = values[3].ToString();
                    DataTable dt = new DataTable();

                    if (!(departamento.Nombre == string.Empty || departamento.Nombre == "desc_dep_inei"))
                    {
                        dt = acceso_datos_departamento.extrae("sp_consultar_ubigeo_departamento_x_nombre", "@nombre", departamento.Nombre);

                        foreach (DataRow row in dt.Rows)
                        {
                            provincia.UbigeoDepartamentoId = int.Parse(row["id"].ToString());
                            break;
                        }

                        if (!(acceso_datos_provincia.existe("sp_consultar_ubigeo_provincia_x_nombre_y_departamento", "@nombre", provincia.Nombre, "@ubigeo_departamento_id", provincia.UbigeoDepartamentoId)) || provincia.Nombre == string.Empty || provincia.Nombre == "desc_prov_inei")
                        {
                            acceso_datos_provincia.crear("sp_insertar_ubigeo_provincia", provincia);
                        }
                    }

                }
            }
        }

        protected void btnAgregarDistritos_Click(object sender, EventArgs e)
        {
            string ruta_archivo_csv = MapPath("~/Content/static/files/equivalencia-ubigeos-oti-concytec.csv");

            using (var fs = File.OpenRead(ruta_archivo_csv))
            using (var reader = new StreamReader(fs))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');

                    departamento.Nombre = values[1].ToString();
                    provincia.Nombre = values[3].ToString();
                    distrito.Nombre = values[5].ToString();
                    DataTable dt = new DataTable();

                    if (!(departamento.Nombre == string.Empty || departamento.Nombre == "desc_dep_inei"))
                    {
                        dt = acceso_datos_departamento.extrae("sp_consultar_ubigeo_departamento_x_nombre", "@nombre", departamento.Nombre);

                        foreach (DataRow row in dt.Rows)
                        {
                            provincia.UbigeoDepartamentoId = int.Parse(row["id"].ToString());
                            break;
                        }

                        dt = acceso_datos_provincia.extrae("sp_consultar_ubigeo_provincia_x_nombre_y_departamento", "@nombre", provincia.Nombre, "@ubigeo_departamento_id", provincia.UbigeoDepartamentoId);

                        foreach (DataRow row in dt.Rows)
                        {
                            distrito.UbigeoProvinciaId = int.Parse(row["id"].ToString());
                            break;
                        }

                        if (!(acceso_datos_distrito.existe("sp_consultar_ubigeo_distrito_x_nombre_y_provincia", "@nombre", distrito.Nombre, "@ubigeo_provincia_id", distrito.UbigeoProvinciaId)) || distrito.Nombre == string.Empty || distrito.Nombre == "desc_ubigeo_inei")
                        {
                            acceso_datos_distrito.crear("sp_insertar_ubigeo_distrito", distrito);
                        }
                    }
                }
            }
        }
    }
}