﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PortalInmobiliario.Entidades;
using PortalInmobiliario.AccesoDatos;

namespace PortalInmobiliario.Presentacion.Mantenimiento
{
    public partial class Proyectos : System.Web.UI.Page
    {
        ADUbigeoDepartamento acceso_datos_departamento = new ADUbigeoDepartamento();
        ADUbigeoProvincia acceso_datos_provincia = new ADUbigeoProvincia();
        ADUbigeoDistrito acceso_datos_distrito = new ADUbigeoDistrito();
        Proyecto proyecto_inmobiliario = new Proyecto();
        ADProyecto acceso_datos_proyecto = new ADProyecto();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ddlUbigeoDepartamentos.DataSource = acceso_datos_departamento.extrae("sp_consultar_ubigeo_departamentos");
                ddlUbigeoDepartamentos.DataTextField = "nombre";
                ddlUbigeoDepartamentos.DataValueField = "id";
                ddlUbigeoDepartamentos.DataBind();
                //ddlProvincias.DataSource = acceso_datos_provincia.extrae("sp_consultar_ubigeo_provincias", "@ubigeo_departamento_id", int.Parse(ddlDepartamentos.SelectedValue.ToString()));
                //ddlProvincias.DataTextField = "nombre";
                //ddlProvincias.DataValueField = "id";
                //ddlProvincias.DataBind();
                //ddlDistritos.DataSource = acceso_datos_distrito.extrae("sp_consultar_ubigeo_distritos", "@ubigeo_provincia_id", int.Parse(ddlProvincias.SelectedValue.ToString()));
                //ddlDistritos.DataTextField = "nombre";
                //ddlDistritos.DataValueField = "id";
                //ddlDistritos.DataBind();
                rellena_grid();
            }
        }

        protected void ddlDepartamentos_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlUbigeoProvincias.DataSource = acceso_datos_provincia.extrae("sp_consultar_ubigeo_provincias", "@ubigeo_departamento_id", int.Parse(ddlUbigeoDepartamentos.SelectedValue.ToString()));
            ddlUbigeoProvincias.DataTextField = "nombre";
            ddlUbigeoProvincias.DataValueField = "id";
            ddlUbigeoProvincias.DataBind();
        }

        protected void ddlProvincias_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlUbigeoDistritos.DataSource = acceso_datos_distrito.extrae("sp_consultar_ubigeo_distritos", "@ubigeo_provincia_id", int.Parse(ddlUbigeoProvincias.SelectedValue.ToString()));
            ddlUbigeoDistritos.DataTextField = "nombre";
            ddlUbigeoDistritos.DataValueField = "id";
            ddlUbigeoDistritos.DataBind();
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            txbNombre.Text = "";
            txbDescripcion.InnerText = "";
            txbPisos.Text = "";
            txbDepartamentos.Text = "";
            txbDireccion.Text = "";
            txbLatitudUbicacion.Text = "";
            txbLongitudUbicacion.Text = "";
            txbAdicionales.InnerText = "";
            txbNombre.Focus();
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            proyecto_inmobiliario.Nombre = txbNombre.Text.ToString();
            proyecto_inmobiliario.Descripcion = txbDescripcion.InnerText.ToString();
            proyecto_inmobiliario.Pisos = int.Parse(txbPisos.Text.ToString());
            proyecto_inmobiliario.Departamentos = int.Parse(txbDepartamentos.Text.ToString());
            proyecto_inmobiliario.Direccion = txbDireccion.Text.ToString();
            proyecto_inmobiliario.Latitud = txbLatitudUbicacion.Text.ToString();
            proyecto_inmobiliario.Longitud = txbLongitudUbicacion.Text.ToString();
            proyecto_inmobiliario.Adicionales = txbAdicionales.InnerText.ToString();
            proyecto_inmobiliario.Estado = ddlEstado.SelectedValue.ToString();
            proyecto_inmobiliario.FechaEntrega = DateTime.Parse(cldFechaEntrega.SelectedDate.ToString());
            proyecto_inmobiliario.UbigeoDistritoId = int.Parse(ddlUbigeoDistritos.SelectedValue.ToString());
            acceso_datos_proyecto.Crear("sp_insertar_proyecto", proyecto_inmobiliario);
            rellena_grid();
        }

        public void rellena_grid()
        {
            dgvProyectos.DataSource = acceso_datos_proyecto.extrae("sp_consultar_proyectos");
            dgvProyectos.DataBind();
        }
    }
}