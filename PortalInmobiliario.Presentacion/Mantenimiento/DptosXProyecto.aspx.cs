﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PortalInmobiliario.Entidades;
using PortalInmobiliario.AccesoDatos;

namespace PortalInmobiliario.Presentacion.Mantenimiento
{
    public partial class DptosXProyecto : System.Web.UI.Page
    {
        ADProyecto acceso_datos_proyecto = new ADProyecto();
        Departamento departamento_x_proyecto = new Departamento();
        ADDepartamento acceso_datos_departamento = new ADDepartamento();

        protected void Page_Load(object sender, EventArgs e)
        {
            ddlProyectos.DataSource = acceso_datos_proyecto.extrae("sp_consultar_proyectos");
            ddlProyectos.DataTextField = "nombre";
            ddlProyectos.DataValueField = "id";
            ddlProyectos.DataBind();
            rellena_grid();
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            txbCodigo.Text = "";
            txbAreaTotal.Text = "";
            txbPrecioM2.Text = "";
            txbPisoUbicado.Text = "";
            txbCodigo.Focus();
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            departamento_x_proyecto.Codigo = txbCodigo.Text.ToString();
            departamento_x_proyecto.AreaTotal = decimal.Parse(txbAreaTotal.Text.ToString());
            departamento_x_proyecto.PrecioM2 = decimal.Parse(txbPrecioM2.Text.ToString());
            departamento_x_proyecto.Moneda = ddlMonedas.SelectedValue.ToString();
            departamento_x_proyecto.PisoUbicado = int.Parse(txbPisoUbicado.Text.ToString());
            departamento_x_proyecto.ProyectoId = int.Parse(ddlProyectos.SelectedValue.ToString());
            acceso_datos_departamento.crear("sp_insertar_departamento_x_proyecto", departamento_x_proyecto);
            rellena_grid();
        }

        public void rellena_grid()
        {
            dgvDepartamentos.DataSource = acceso_datos_proyecto.extrae("sp_consultar_departamentos_x_proyecto", "@proyecto_id", int.Parse(ddlProyectos.SelectedValue.ToString()));
            dgvDepartamentos.DataBind();
        }
    }
}