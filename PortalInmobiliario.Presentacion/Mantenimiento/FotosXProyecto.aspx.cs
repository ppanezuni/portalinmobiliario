﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PortalInmobiliario.Entidades;
using PortalInmobiliario.AccesoDatos;

namespace Portal_Inmobilario.Capa.Presentacion.Mantenimiento
{
    public partial class FotosXProyecto : System.Web.UI.Page
    {
        ADProyecto acceso_datos_proyecto = new ADProyecto();
        FotoXProyecto foto_proyecto = new FotoXProyecto();
        ADFotoXProyecto acceso_datos_foto_proyecto = new ADFotoXProyecto();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ddlProyectos.DataSource = acceso_datos_proyecto.extrae("sp_consultar_proyectos");
                ddlProyectos.DataTextField = "nombre";
                ddlProyectos.DataValueField = "id";
                ddlProyectos.DataBind();
                rellena_grid();
            }
        }

        protected void btnSubir_Click(object sender, EventArgs e)
        {
            if (fupFoto.HasFile)
            {
                string file = fupFoto.FileName;
                fupFoto.SaveAs(MapPath("~/Content/media/img/proyectos/" + file));
                imgFoto.ImageUrl = "~/Content/media/img/proyectos/" + file;
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            var ruta = imgFoto.ImageUrl.ToString();
            foto_proyecto.Ruta = ruta.Substring(1);
            foto_proyecto.ProyectoId = int.Parse(ddlProyectos.SelectedValue.ToString());
            acceso_datos_foto_proyecto.crear("sp_insertar_foto_x_proyecto", foto_proyecto);
            rellena_grid();
        }

        public void rellena_grid()
        {
            gdvFotos.DataSource = acceso_datos_foto_proyecto.extrae("sp_consultar_fotos_x_proyecto");
            gdvFotos.DataBind();
        }
    }
}