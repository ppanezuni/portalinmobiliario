﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PortalInmobiliario.Entidades;
using PortalInmobiliario.AccesoDatos;

namespace PortalInmobiliario.Presentacion.Mantenimiento
{
    public partial class EntidadesFinancieras : System.Web.UI.Page
    {
        EntidadFinanciera financiera = new EntidadFinanciera();
        ADEntidadFinanciera acceso_datos_financiera = new ADEntidadFinanciera();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                rellena_grid();
            }
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            txbNombre.Text = "";
            txbCuotaInicial.Text = "";
            txbTasaInteres.Text = "";
            txbNombre.Focus();
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            financiera.Nombre = txbNombre.Text;
            financiera.CuotaInicial = Decimal.Parse(txbCuotaInicial.Text.ToString());
            financiera.TasaInteres = Decimal.Parse(txbTasaInteres.Text.ToString());
            acceso_datos_financiera.crear("sp_insertar_entidad_financiera", financiera);
            rellena_grid();
        }

        public void rellena_grid()
        {
            dgvEntidades.DataSource = acceso_datos_financiera.extrae("sp_consultar_entidades_financieras");
            dgvEntidades.DataBind();
        }
    }
}