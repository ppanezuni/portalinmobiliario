﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PortalInmobiliario.Entidades;
using PortalInmobiliario.AccesoDatos;

namespace PortalInmobiliario.Presentacion.Mantenimiento
{
    public partial class TiposHabitacion : System.Web.UI.Page
    {
        TipoHabitacion tipo = new TipoHabitacion();
        ADTipoHabitacion acceso_datos_tipo = new ADTipoHabitacion();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                rellena_grid();
            }
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            txbNombre.Text = "";
            txbNombre.Focus();
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            tipo.Nombre = txbNombre.Text;
            acceso_datos_tipo.crear("sp_insertar_tipo_habitacion", tipo);
            rellena_grid();
        }

        public void rellena_grid()
        {
            dgvTiposHab.DataSource = acceso_datos_tipo.extrae("sp_consultar_tipos_habitacion");
            dgvTiposHab.DataBind();
        }
    }
}