﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PortalInmobiliario.AccesoDatos;
using PortalInmobiliario.Entidades;
using System.Data;

namespace PortalInmobiliario.Presentacion
{
    public partial class ReservarDepartamento : System.Web.UI.Page
    {
        ADCliente AccesoDatosCliente = new ADCliente();
        ADDepartamento AccesoDatosDepartamento = new ADDepartamento();
        ADReserva AccesoDatosReserva = new ADReserva();
        Cliente cliente = new Cliente();
        Departamento departamento = new Departamento();
        Reserva reserva = new Reserva();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

            }
        }

        protected void btnReservar_Click(object sender, EventArgs e)
        {
            int departmentID = Int32.Parse(Request.QueryString["id"]);

            cliente.Dni = txbDNI.Text;
            cliente.ApellidoPaterno = txbApellidoPaterno.Text;
            cliente.ApellidoMaterno = txbApellidoMaterno.Text;
            cliente.Nombres = txbNombres.Text;
            cliente.Telefono = txbTelefono.Text;
            cliente.CorreoElectronico = txbCorreoElectronico.Text;

            if (!AccesoDatosCliente.existe("sp_consultar_cliente_x_nombre_completo", "@nombres", cliente.Nombres, "@apellido_paterno", cliente.ApellidoPaterno, "@apellido_materno", cliente.ApellidoMaterno, "@dni", cliente.Dni))
            {
                AccesoDatosCliente.crear("sp_insertar_cliente", cliente);
            }

            DataTable dt = AccesoDatosCliente.extrae("sp_consultar_cliente_x_nombre_completo", "@nombres", cliente.Nombres, "@apellido_paterno", cliente.ApellidoPaterno, "@apellido_materno", cliente.ApellidoMaterno, "@dni", cliente.Dni);
            DataTable dt1 = AccesoDatosDepartamento.extrae("sp_consultar_departamento_x_id", "@id", departmentID);

            foreach (DataRow row in dt.Rows)
            {
                foreach (DataRow row1 in dt1.Rows)
                {
                    departamento.Id = Int32.Parse(row1["id"].ToString());
                    departamento.Codigo = row1["codigo"].ToString();
                    departamento.AreaTotal = Decimal.Parse(row1["area_total"].ToString());
                    departamento.PrecioM2 = Decimal.Parse(row1["precio_m2"].ToString());
                    departamento.Moneda = row1["moneda"].ToString();
                    departamento.PisoUbicado = Int32.Parse(row1["piso_ubicado"].ToString());
                    departamento.Reservado = true;
                    departamento.ProyectoId = Int32.Parse(row1["proyecto_id"].ToString());
                    AccesoDatosDepartamento.actualizar("sp_editar_departamento", departamento);
                    reserva.ClienteId = Int32.Parse(row["id"].ToString());
                    reserva.DepartamentoId = departamento.Id;
                    reserva.fechaReserva = DateTime.Now;
                    AccesoDatosReserva.crear("sp_insertar_reserva", reserva);
                }
            }
        }
    }
}