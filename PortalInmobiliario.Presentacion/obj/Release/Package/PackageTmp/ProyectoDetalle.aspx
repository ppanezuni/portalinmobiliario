﻿<%@ Page Title="Detalle de Proyecto" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProyectoDetalle.aspx.cs" Inherits="PortalInmobiliario.Presentacion.ProyectoDetalle" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content"><div class="ic">More Website Templates @ TemplateMonster.com - June 02, 2014!</div>
        <div class="container">
            <div class="row">
                <form runat="server">
                    <div class="grid_7">
                        <asp:FormView ID="fvProyecto" runat="server">
                            <ItemTemplate>
                                <h3><%# Eval("nombre") %></h3>
                                <div class="blog">
                                    <!-- <time datetime="2014-01-01"><span class="fa fa-calendar"></span>21 May
                                    </time> -->
                                    <div class="extra_wrapper">
                                        <div class="text1"><a href="#"><%# Eval("estado") %></a></div>
                                    </div>
                                    <div class="clear"></div>
                                    <img src="/Content/static/img/page4_img1.jpg" alt="" class="img_inner fleft">
                                    <div class="extra_wrapper">
                                        Descripción del Proyecto: 
                                        <br>
                                        <br>
                                        <p align="justify">
                                            <%# Eval("descripcion") %>
                                        </p>
                                        <!-- <a href="#" class="btn">Departamentos disponibles</a> -->
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:FormView>
                    </div>
                    <div class="grid_4 preffix_1">
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <h3>Departamentos disponibles</h3>
                        <asp:Label ID="Label1" runat="server" Text="Elija un piso: "></asp:Label>
                        <asp:CheckBoxList ID="CheckBoxList1" runat="server">
                        </asp:CheckBoxList>
                        &nbsp;<asp:DropDownList ID="ddlPisos" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlPisos_SelectedIndexChanged">
                        </asp:DropDownList>
                        <br>
                        <br>
                        <ul style="list-style-type: circle;">
                            <asp:DataList ID="dtlDepartamentos" runat="server">
                                <ItemTemplate>
                                    <li>
                                        <a href="/DepartamentoDetalle?id=<%# Eval("id") %>"><%# Eval("codigo") %></a>
                                    </li>
                                </ItemTemplate>
                            </asp:DataList>
                        </ul>
                        <br>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <script>
        document.getElementById("ProjectsOption").className = "current";
    </script>
</asp:Content>
