﻿<%@ Page Title="Calculadora de Cuotas" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CalcularCuotas.aspx.cs" Inherits="PortalInmobiliario.Presentacion.CalcularCuotas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content"><div class="ic">More Website Templates @ TemplateMonster.com - June 02, 2014!</div>
        <div class="container">
            <div class="row">
                <div class="grid_7">
                    <h3>Calculadora de Cuotas</h3>
                    <form id="form" runat="server">
                        <br>
                        <asp:Label ID="Label1" runat="server" Text="Precio de venta del inmueble: "></asp:Label>
                        <asp:TextBox ID="txbPrecio" runat="server" Enabled="False"></asp:TextBox>
                        <br>
                        <br>
                        <asp:Label ID="Label6" runat="server" Text="Entidad Financiera: "></asp:Label>
                        <asp:DropDownList ID="ddlFinancieras" class="form-control" runat="server" required OnSelectedIndexChanged="ddlFinancieras_SelectedIndexChanged" AutoPostBack="True">
                        </asp:DropDownList>
                        <br>
                        <br>
                        <asp:Label ID="Label2" runat="server" Text="Monto de cuota incial: "></asp:Label>
                        <asp:TextBox ID="txbCuotaInicial" runat="server" Enabled="False"></asp:TextBox>
                        <br>
                        <br>
                        <asp:Label ID="Label3" runat="server" Text="Monto del financiamiento: "></asp:Label>
                        <asp:TextBox ID="txbFinanciamiento" runat="server" Enabled="False"></asp:TextBox>
                        <br>
                        <br>
                        <asp:Label ID="Label4" runat="server" Text="Tasa de Interés (%): "></asp:Label>
                        <asp:TextBox ID="txbInteres" runat="server" Enabled="False"></asp:TextBox>
                        <br>
                        <br>
                        <asp:Label ID="Label5" runat="server" Text="Años a Pagar: "></asp:Label>
                        <asp:TextBox ID="txbAños" runat="server"></asp:TextBox>
                        <br>
                        <br>
                        <div>
                            <div class="clear"></div>
                            <div class="btns">
                                <asp:LinkButton ID="btnCalcular" type="submit" class="btn" runat="server" OnClick="btnCalcular_Click">Calcular</asp:LinkButton>
                            </div>
                        </div>
                        <br>
                        <br>
                        <br>
                        <asp:Label ID="lblCuotas" runat="server" Text=""></asp:Label>
                    </form>
                </div>
                <div class="grid_4 preffix_1">
                    
                </div>
            </div>
        </div>
    </section>
    <script>
        document.getElementById("ProjectsOption").className = "current";
    </script>
</asp:Content>
