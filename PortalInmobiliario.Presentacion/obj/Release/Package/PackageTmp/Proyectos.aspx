﻿<%@ Page Title="Proyectos" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Proyectos.aspx.cs" Inherits="PortalInmobiliario.Presentacion.Proyectos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content gallery">
		<div class="ic">More Website Templates @ TemplateMonster.com - June 02, 2014!</div>
  		<div class="container">
    		<div class="row">
      			<div class="grid_12">
        			<h3>Proyectos Inmobiliarios</h3>
      			</div>
                <asp:DataList ID="dtlProyectos" runat="server" RepeatColumns="3" RepeatDirection="Horizontal">
                    <ItemTemplate>
                        <div class="grid_4">
        			        <div class="box">
          				        <div class="maxheight">
            				        <a href="/Content/static/img/big1.jpg" class="gal">
            					        <img src="/Content/static/img/page2_img1.jpg" alt="foto_proyecto">
            				        </a>
            				        &nbsp;&nbsp;<div class="box_bot">
            					        <div class="text1">
            						        <%# Eval("nombre") %>
                                            <p><strong>Desde S/. 435 612</strong></p>
                                            <p><%# Eval("ubigeo") %></p>
            					        </div>
            					        <a href="/ProyectoDetalle?id=<%# Eval("id") %>" class="fa fa-chevron-right"></a>
            				        </div>
          				        </div>
        			        </div>
      			        </div>
                    </ItemTemplate>
                </asp:DataList>
                <div class="clear ind__1"></div>
    		</div>
  		</div>
	</section>
	<script>
        document.getElementById("ProjectsOption").className = "current";
    </script>
</asp:Content>
