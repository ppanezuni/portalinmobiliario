﻿<%@ Page Title="Dashboard | Mantenimiento Proyectos" Language="C#" MasterPageFile="~/Dashboard.master" AutoEventWireup="true" CodeBehind="Proyectos.aspx.cs" Inherits="PortalInmobiliario.Presentacion.Mantenimiento.Proyectos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <h1>MANTENIMIENTO DE PROYECTOS INMOBILIARIOS</h1>
    <br />
    <div class="form-group row col-sm-12">
        <div class="col-sm-2">
            <asp:Label ID="Label1" runat="server" Text="Nombre: "></asp:Label>
        </div>
        <div class="col-sm-10">
            <asp:TextBox ID="txbNombre" class="form-control" runat="server" MaxLength="64" required></asp:TextBox>
        </div>
    </div>
    <div class="form-group row col-sm-12">
        <div class="col-sm-2">
            <asp:Label ID="Label2" runat="server" Text="Descripción: "></asp:Label>
        </div>
        <div class="col-sm-10">
           <%-- <asp:TextBox ID="txbDescripcion" TextMode="multiline" class="form-control" runat="server" Columns="50" Rows="30" required></asp:TextBox>--%>
           <textarea name="" id="txbDescripcion" class="form-control" runat="server" cols="50" rows="5" required></textarea>
        </div>
    </div>
    <div class="form-group row col-sm-12">
        <div class="col-sm-2">
            <asp:Label ID="Label3" runat="server" Text="Cantidad de Pisos: "></asp:Label>
        </div>
        <div class="col-sm-10">
            <asp:TextBox ID="txbPisos" class="form-control" runat="server" required></asp:TextBox>
        </div>
    </div>
    <div class="form-group row col-sm-12">
        <div class="col-sm-2">
            <asp:Label ID="Label4" runat="server" Text="Cantidad de Departamentos: "></asp:Label>
        </div>
        <div class="col-sm-10">
            <asp:TextBox ID="txbDepartamentos" class="form-control" runat="server" required></asp:TextBox>
        </div>
    </div>
    <div class="form-group row col-sm-12">
        <div class="col-sm-2">
            <asp:Label ID="Label5" runat="server" Text="Dirección: "></asp:Label>
        </div>
        <div class="col-sm-10">
            <asp:TextBox ID="txbDireccion" class="form-control" runat="server" MaxLength="64" required></asp:TextBox>
        </div>
    </div>
    <div class="form-group row col-sm-12">
        <div class="col-sm-2">
            <asp:Label ID="Label6" runat="server" Text="Latitud: "></asp:Label>
        </div>
        <div class="col-sm-10">
            <asp:TextBox ID="txbLatitudUbicacion" class="form-control" runat="server" MaxLength="32" required></asp:TextBox>
        </div>
    </div>
    <div class="form-group row col-sm-12">
        <div class="col-sm-2">
            <asp:Label ID="Label7" runat="server" Text="Longitud: "></asp:Label>
        </div>
        <div class="col-sm-10">
            <asp:TextBox ID="txbLongitudUbicacion" class="form-control" runat="server" MaxLength="32" required></asp:TextBox>
        </div>
    </div>
    <div class="form-group row col-sm-12">
        <div class="col-sm-2">
            <asp:Label ID="Label8" runat="server" Text="Adicionales: "></asp:Label>
        </div>
        <div class="col-sm-10">
           <%-- <asp:TextBox ID="txbAdicionales" TextMode="multiline" class="form-control" runat="server" Columns="50" Rows="30" required></asp:TextBox>--%>
           <textarea name="" id="txbAdicionales" class="form-control" runat="server" cols="50" rows="5" required></textarea>
        </div>
    </div>
    <div class="form-group row col-sm-12">
        <div class="col-sm-2">
            <asp:Label ID="Label9" runat="server" Text="Estado: "></asp:Label>
        </div>
        <div class="col-sm-4">
            <asp:DropDownList ID="ddlEstado" class="form-control" runat="server" required>
                <asp:ListItem>EN CONSTRUCCIÓN</asp:ListItem>
                <asp:ListItem>EN PLANOS</asp:ListItem>
                <asp:ListItem>ESTRENO</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div class="form-group row col-sm-12">
        <div class="col-sm-2">
            <asp:Label ID="Label10" runat="server" Text="Fecha de Entrega: "></asp:Label>
        </div>
        <div class="col-sm-10">
           <asp:Calendar ID="cldFechaEntrega" runat="server"></asp:Calendar>
        </div>
    </div>
    <div class="form-group row col-sm-12">
        <div class="col-sm-2">
            <asp:Label ID="Label11" runat="server" Text="Departamento: "></asp:Label>
        </div>
        <div class="col-sm-4">
            <asp:DropDownList ID="ddlUbigeoDepartamentos" class="form-control" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlDepartamentos_SelectedIndexChanged" required>
            </asp:DropDownList>
        </div>
    </div>
    <div class="form-group row col-sm-12">
        <div class="col-sm-2">
            <asp:Label ID="Label12" runat="server" Text="Provincia: "></asp:Label>
        </div>
        <div class="col-sm-4">
            <asp:DropDownList ID="ddlUbigeoProvincias" class="form-control" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlProvincias_SelectedIndexChanged" required>
            </asp:DropDownList>
        </div>
    </div>
    <div class="form-group row col-sm-12">
        <div class="col-sm-2">
            <asp:Label ID="Label13" runat="server" Text="Distrito: "></asp:Label>
        </div>
        <div class="col-sm-4">
            <asp:DropDownList ID="ddlUbigeoDistritos" class="form-control" runat="server" required>
            </asp:DropDownList>
        </div>
    </div>
    <br />
    <asp:Button ID="btnNuevo" type="reset" class="btn btn-default" runat="server" OnClick="btnNuevo_Click" Text="Nuevo" />
    <asp:Button ID="btnGuardar" type="submit" class="btn btn-success" runat="server" OnClick="btnGuardar_Click" Text="Guardar" />
    <br />
    <br />
    <asp:GridView class="table table-bordered bs-table" ID="dgvProyectos" runat="server">
    </asp:GridView>
</asp:Content>