﻿<%@ Page Title="Dashboard | Mantenimiento Clientes" Language="C#" MasterPageFile="~/Dashboard.master" AutoEventWireup="true" CodeBehind="Clientes.aspx.cs" Inherits="PortalInmobiliario.Presentacion.Mantenimiento.Clientes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <h1>MANTENIMIENTO DE CLIENTES</h1>
    <br />
    <!--<div class="form-group row col-sm-12">
        <div class="col-sm-2">
            <asp:Label ID="Label1" runat="server" Text="DNI: "></asp:Label>
        </div>
        <div class="col-sm-10">
            <asp:TextBox ID="txbDNI" class="form-control" runat="server" MaxLength="8" required></asp:TextBox>
        </div>
    </div>
    <div class="form-group row col-sm-12">
        <div class="col-sm-2">
            <asp:Label ID="Label2" runat="server" Text="Apellido Paterno: "></asp:Label>
        </div>
        <div class="col-sm-10">
            <asp:TextBox ID="txbApellidoPaterno" class="form-control" runat="server" MaxLength="16" required></asp:TextBox>
        </div>
    </div>
    <div class="form-group row col-sm-12">
        <div class="col-sm-2">
            <asp:Label ID="Label3" runat="server" Text="Apellido Materno: "></asp:Label>
        </div>
        <div class="col-sm-10">
            <asp:TextBox ID="txbApellidoMaterno" class="form-control" runat="server" MaxLength="16" required></asp:TextBox>
        </div>
    </div>
    <div class="form-group row col-sm-12">
        <div class="col-sm-2">
            <asp:Label ID="Label4" runat="server" Text="Nombres: "></asp:Label>
        </div>
        <div class="col-sm-10">
            <asp:TextBox ID="txbNombres" class="form-control" runat="server" MaxLength="16" required></asp:TextBox>
        </div>
    </div>
    <div class="form-group row col-sm-12">
        <div class="col-sm-2">
            <asp:Label ID="Label5" runat="server" Text="Teléfono: "></asp:Label>
        </div>
        <div class="col-sm-10">
            <asp:TextBox ID="txbTelefono" class="form-control" runat="server" MaxLength="16" required></asp:TextBox>
        </div>
    </div>
    <div class="form-group row col-sm-12">
        <div class="col-sm-2">
            <asp:Label ID="Label6" runat="server" Text="E - mail: "></asp:Label>
        </div>
        <div class="col-sm-10">
            <asp:TextBox ID="txbCorreoElectronico" class="form-control" type="email" runat="server" MaxLength="64"></asp:TextBox>
        </div>
    </div>
    <br />
    <asp:Button ID="btnNuevo" type="reset" class="btn btn-default" runat="server" OnClick="btnNuevo_Click" Text="Nuevo" />
    <asp:Button ID="btnGuardar" type="submit" class="btn btn-success" runat="server" OnClick="btnGuardar_Click" Text="Guardar" />
    <br />
    <br />-->
    <asp:GridView class="table table-bordered bs-table" ID="dgvClientes" runat="server">
    </asp:GridView>
</asp:Content>