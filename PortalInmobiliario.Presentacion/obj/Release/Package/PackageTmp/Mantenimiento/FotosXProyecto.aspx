﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dashboard.master" AutoEventWireup="true" CodeBehind="FotosXProyecto.aspx.cs" Inherits="Portal_Inmobilario.Capa.Presentacion.Mantenimiento.FotosXProyecto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <h1>MANTENIMIENTO DE FOTOS DE PROYECTOS INMOBILIARIOS</h1>
    <br />
    <div class="form-group row col-sm-12">
        <div class="col-sm-2">
            <asp:Label ID="Label1" runat="server" Text="Proyecto: "></asp:Label>
        </div>
        <div class="col-sm-4">
            <asp:DropDownList ID="ddlProyectos" class="form-control" runat="server">
            </asp:DropDownList>
        </div>
    </div>
    <div class="form-group row col-sm-12">
        <div class="col-sm-6">
            <asp:CheckBox ID="ckbPortada" class="form-control" runat="server" Text="¿Es foto de portada?" required/>
        </div>
    </div>
    <div class="form-group row col-sm-12">
        <div class="col-sm-2">
            <asp:Label ID="Label2" runat="server" Text="Foto: "></asp:Label>
        </div>
        <div class="col-sm-4">
            <asp:Image ID="imgFoto" runat="server" Height="121px" Width="98px" />
            <asp:FileUpload class="form-control" ID="fupFoto" runat="server" />
            <br />
            <asp:Button ID="btnSubir" type="submit" class="btn btn-primary" runat="server" OnClick="btnSubir_Click" Text="Subir Foto" />
        </div>
    </div>
    <br />
    <asp:Button ID="btnGuardar" type="submit" class="btn btn-success" runat="server" OnClick="btnGuardar_Click" Text="Guardar" />
    <br />
    <br />
    <asp:GridView class="table table-bordered bs-table" ID="gdvFotos" runat="server">
    </asp:GridView>
</asp:Content>
