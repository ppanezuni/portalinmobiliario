﻿<%@ Page Title="Dashboard | Mantenimiento Financieras" Language="C#" MasterPageFile="~/Dashboard.master" AutoEventWireup="true" CodeBehind="EntidadesFinancieras.aspx.cs" Inherits="PortalInmobiliario.Presentacion.Mantenimiento.EntidadesFinancieras" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <h1>MANTENIMIENTO DE ENTIDADES FINANCIERAS</h1>
    <br />
    <div class="form-group row col-sm-12">
        <div class="col-sm-2">
            <asp:Label ID="Label1" runat="server" Text="Nombre: "></asp:Label>
        </div>
        <div class="col-sm-10">
            <asp:TextBox ID="txbNombre" class="form-control" runat="server" MaxLength="32" required></asp:TextBox>
        </div>
    </div>
    <div class="form-group row col-sm-12">
        <div class="col-sm-2">
            <asp:Label ID="Label2" runat="server" Text="Cuota Inicial: "></asp:Label>
        </div>
        <div class="col-sm-10">
            <asp:TextBox ID="txbCuotaInicial" class="form-control" runat="server" required></asp:TextBox>
        </div>
    </div>
    <div class="form-group row col-sm-12">
        <div class="col-sm-2">
            <asp:Label ID="Label3" runat="server" Text="Tasa de Interes (%): "></asp:Label>
        </div>
        <div class="col-sm-10">
            <asp:TextBox ID="txbTasaInteres" class="form-control" runat="server" required></asp:TextBox>
        </div>
    </div>
    <br />
    <asp:Button ID="btnNuevo" type="reset" class="btn btn-default" runat="server" OnClick="btnNuevo_Click" Text="Nuevo" />
    <asp:Button ID="btnGuardar" type="submit" class="btn btn-success" runat="server" OnClick="btnGuardar_Click" Text="Guardar" />
    <br />
    <br />
    <asp:GridView class="table table-bordered bs-table" ID="dgvEntidades" runat="server">
    </asp:GridView>
</asp:Content>