﻿<%@ Page Title="Dashboard | Mantenimiento Ubigeo" Language="C#" MasterPageFile="~/Dashboard.master" AutoEventWireup="true" CodeBehind="Ubigeo.aspx.cs" Inherits="PortalInmobiliario.Presentacion.Mantenimiento.Ubigeo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <h1>MANTENIMIENTO DEL UBIGEO</h1>
    <br />
    <asp:Button ID="btnAgregarDepartamentos" type="submit" class="btn btn-default" runat="server" OnClick="btnAgregarDepartamentos_Click" Text="Agregar Departamentos" />
    <br />
    <br />
    <asp:Button ID="btnAgregarProvincias" type="submit" class="btn btn-default" runat="server" OnClick="btnAgregarProvincias_Click" Text="Agregar Provincias" />
    <br />
    <br />
    <asp:Button ID="btnAgregarDistritos" type="submit" class="btn btn-default" runat="server" Text="Agregar Distritos" OnClick="btnAgregarDistritos_Click" />
</asp:Content>