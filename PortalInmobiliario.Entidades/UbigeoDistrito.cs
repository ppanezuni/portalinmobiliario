﻿namespace PortalInmobiliario.Entidades
{
    public class UbigeoDistrito
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int UbigeoProvinciaId { get; set; }
    }
}
