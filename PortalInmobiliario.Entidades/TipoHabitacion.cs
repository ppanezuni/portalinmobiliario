﻿namespace PortalInmobiliario.Entidades
{
    public class TipoHabitacion
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}
