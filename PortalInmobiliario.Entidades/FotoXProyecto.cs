﻿namespace PortalInmobiliario.Entidades
{
    public class FotoXProyecto
    {
        public int Id { get; set; }
        public bool Portada { get; set; }
        public string Ruta { get; set; }
        public int ProyectoId { get; set; }
    }
}
