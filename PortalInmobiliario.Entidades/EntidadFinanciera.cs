﻿namespace PortalInmobiliario.Entidades
{
    public class EntidadFinanciera
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public decimal CuotaInicial { get; set; }
        public decimal TasaInteres { get; set; }
    }
}
