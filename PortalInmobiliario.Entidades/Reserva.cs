﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalInmobiliario.Entidades
{
    public class Reserva
    {
        public int Id { get; set; }
        public int DepartamentoId { get; set; }
        public int ClienteId { get; set; }
        public DateTime fechaReserva { get; set; }
    }
}
