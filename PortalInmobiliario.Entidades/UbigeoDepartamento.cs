﻿namespace PortalInmobiliario.Entidades
{
    public class UbigeoDepartamento
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}
