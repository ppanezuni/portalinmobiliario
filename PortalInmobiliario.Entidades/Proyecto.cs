﻿using System;

namespace PortalInmobiliario.Entidades
{
    public class Proyecto
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public int Pisos { get; set; }
        public int Departamentos { get; set; }
        public string Direccion { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public string Adicionales { get; set; }
        public string Estado { get; set; }
        public DateTime FechaEntrega { get; set; }
        public int UbigeoDistritoId { get; set; }
    }
}
