﻿namespace PortalInmobiliario.Entidades
{
    public class Persona
    {
        public string Dni { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Nombres { get; set; }
        public string Telefono { get; set; }
        public string CorreoElectronico { get; set; }
    }
}
