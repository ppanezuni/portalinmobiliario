﻿namespace PortalInmobiliario.Entidades
{
    public class Departamento
    {
        public int Id { get; set; }
        public string Codigo { get; set; }
        public decimal AreaTotal { get; set; }
        public decimal PrecioM2 { get; set; }
        public string Moneda { get; set; }
        public int PisoUbicado { get; set; }
        public bool Reservado { get; set; }
        public int ProyectoId { get; set; }
    }
}
