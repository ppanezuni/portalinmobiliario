﻿using System.Data;
using System.Data.SqlClient;
using PortalInmobiliario.Entidades;

namespace PortalInmobiliario.AccesoDatos
{
    public class ADProyecto : Consulta
    {
        public void Crear(string nombreProcedimiento, Proyecto proyectoInmobiliario)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexion.cadena;
            cmd.CommandText = nombreProcedimiento;
            cmd.Parameters.Add("@nombre", SqlDbType.VarChar).Value = proyectoInmobiliario.Nombre;
            cmd.Parameters.Add("@descripcion", SqlDbType.Text).Value = proyectoInmobiliario.Descripcion;
            cmd.Parameters.Add("@pisos", SqlDbType.Int).Value = proyectoInmobiliario.Pisos;
            cmd.Parameters.Add("@departamentos", SqlDbType.Int).Value = proyectoInmobiliario.Departamentos;
            cmd.Parameters.Add("@direccion", SqlDbType.VarChar).Value = proyectoInmobiliario.Direccion;
            cmd.Parameters.Add("@latitud_ubicacion", SqlDbType.VarChar).Value = proyectoInmobiliario.Latitud;
            cmd.Parameters.Add("@longitud_ubicacion", SqlDbType.VarChar).Value = proyectoInmobiliario.Longitud;
            cmd.Parameters.Add("@adicionales", SqlDbType.Text).Value = proyectoInmobiliario.Adicionales;
            cmd.Parameters.Add("@estado", SqlDbType.VarChar).Value = proyectoInmobiliario.Estado;
            cmd.Parameters.Add("@fecha_entrega", SqlDbType.DateTime).Value = proyectoInmobiliario.FechaEntrega;
            cmd.Parameters.Add("@ubigeo_distrito_id", SqlDbType.Int).Value = proyectoInmobiliario.UbigeoDistritoId;
            cmd.CommandType = CommandType.StoredProcedure;
            conexion.Abrir();
            cmd.ExecuteNonQuery();
            conexion.Cerrar();
        }
    }
}
