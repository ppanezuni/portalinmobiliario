﻿using System.Data;
using System.Data.SqlClient;
using PortalInmobiliario.Entidades;

namespace PortalInmobiliario.AccesoDatos
{
    public class ADEntidadFinanciera : Consulta
    {
        public void crear(string nombre_procedimiento, EntidadFinanciera financiera)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexion.cadena;
            cmd.CommandText = nombre_procedimiento;
            cmd.Parameters.Add("@nombre", SqlDbType.VarChar).Value = financiera.Nombre;
            cmd.Parameters.Add("@cuota_inicial", SqlDbType.Decimal).Value = financiera.CuotaInicial;
            cmd.Parameters.Add("@tasa_interes", SqlDbType.Decimal).Value = financiera.TasaInteres;
            cmd.CommandType = CommandType.StoredProcedure;
            conexion.Abrir();
            cmd.ExecuteNonQuery();
            conexion.Cerrar();
        }
    }
}
