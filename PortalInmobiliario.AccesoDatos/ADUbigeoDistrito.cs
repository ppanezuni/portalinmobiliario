﻿using System.Data;
using System.Data.SqlClient;
using PortalInmobiliario.Entidades;

namespace PortalInmobiliario.AccesoDatos
{
    public class ADUbigeoDistrito : Consulta
    {
        public void crear(string nombre_procedimiento, UbigeoDistrito distrito)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexion.cadena;
            cmd.CommandText = nombre_procedimiento;
            cmd.Parameters.Add("@nombre", SqlDbType.VarChar).Value = distrito.Nombre;
            cmd.Parameters.Add("@ubigeo_provincia_id", SqlDbType.Int).Value = distrito.UbigeoProvinciaId;
            cmd.CommandType = CommandType.StoredProcedure;
            conexion.Abrir();
            cmd.ExecuteNonQuery();
            conexion.Cerrar();
        }
    }
}
