﻿using System.Data;
using System.Data.SqlClient;

namespace PortalInmobiliario.AccesoDatos
{
    public class Consulta
    {
        public Conexion conexion = new Conexion();

        public DataTable extrae(string nombre_procedimiento)
        {
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand();
            DataTable dt = new DataTable();
            cmd.Connection = conexion.cadena;
            cmd.CommandText = nombre_procedimiento;
            cmd.CommandType = CommandType.StoredProcedure;
            conexion.Abrir();
            da.SelectCommand = cmd;
            da.Fill(dt);
            conexion.Cerrar();
            return dt;
        }

        public DataTable extrae(string nombre_procedimiento, string nombre_parametro, int valor_parametro)
        {
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand();
            DataTable dt = new DataTable();
            cmd.Connection = conexion.cadena;
            cmd.CommandText = nombre_procedimiento;
            cmd.Parameters.Add(nombre_parametro, SqlDbType.Int).Value = valor_parametro;
            cmd.CommandType = CommandType.StoredProcedure;
            conexion.Abrir();
            da.SelectCommand = cmd;
            da.Fill(dt);
            conexion.Cerrar();
            return dt;
        }

        public DataTable extrae(string nombre_procedimiento, string nombre_parametro, string valor_parametro)
        {
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand();
            DataTable dt = new DataTable();
            cmd.Connection = conexion.cadena;
            cmd.CommandText = nombre_procedimiento;
            cmd.Parameters.Add(nombre_parametro, SqlDbType.VarChar).Value = valor_parametro;
            cmd.CommandType = CommandType.StoredProcedure;
            conexion.Abrir();
            da.SelectCommand = cmd;
            da.Fill(dt);
            conexion.Cerrar();
            return dt;
        }

        public DataTable extrae(string nombre_procedimiento, string nombre_parametro1, string valor_parametro1, string nombre_parametro2, string valor_parametro2)
        {
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand();
            DataTable dt = new DataTable();
            cmd.Connection = conexion.cadena;
            cmd.CommandText = nombre_procedimiento;
            cmd.Parameters.Add(nombre_parametro1, SqlDbType.VarChar).Value = valor_parametro1;
            cmd.Parameters.Add(nombre_parametro2, SqlDbType.VarChar).Value = valor_parametro2;
            cmd.CommandType = CommandType.StoredProcedure;
            conexion.Abrir();
            da.SelectCommand = cmd;
            da.Fill(dt);
            conexion.Cerrar();
            return dt;
        }

        public DataTable extrae(string nombre_procedimiento, string nombre_parametro1, string valor_parametro1, string nombre_parametro2, int valor_parametro2)
        {
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand();
            DataTable dt = new DataTable();
            cmd.Connection = conexion.cadena;
            cmd.CommandText = nombre_procedimiento;
            cmd.Parameters.Add(nombre_parametro1, SqlDbType.VarChar).Value = valor_parametro1;
            cmd.Parameters.Add(nombre_parametro2, SqlDbType.Int).Value = valor_parametro2;
            cmd.CommandType = CommandType.StoredProcedure;
            conexion.Abrir();
            da.SelectCommand = cmd;
            da.Fill(dt);
            conexion.Cerrar();
            return dt;
        }

        public DataTable extrae(string nombre_procedimiento, string nombre_parametro1, int valor_parametro1, string nombre_parametro2, int valor_parametro2)
        {
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand();
            DataTable dt = new DataTable();
            cmd.Connection = conexion.cadena;
            cmd.CommandText = nombre_procedimiento;
            cmd.Parameters.Add(nombre_parametro1, SqlDbType.Int).Value = valor_parametro1;
            cmd.Parameters.Add(nombre_parametro2, SqlDbType.Int).Value = valor_parametro2;
            cmd.CommandType = CommandType.StoredProcedure;
            conexion.Abrir();
            da.SelectCommand = cmd;
            da.Fill(dt);
            conexion.Cerrar();
            return dt;
        }

        public DataTable extrae(string nombre_procedimiento, string nombre_parametro1, string valor_parametro1, string nombre_parametro2, string valor_parametro2, string nombre_parametro3, string valor_parametro3, string nombre_parametro4, string valor_parametro4)
        {
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand();
            DataTable dt = new DataTable();
            cmd.Connection = conexion.cadena;
            cmd.CommandText = nombre_procedimiento;
            cmd.Parameters.Add(nombre_parametro1, SqlDbType.VarChar).Value = valor_parametro1;
            cmd.Parameters.Add(nombre_parametro2, SqlDbType.VarChar).Value = valor_parametro2;
            cmd.Parameters.Add(nombre_parametro3, SqlDbType.VarChar).Value = valor_parametro3;
            cmd.Parameters.Add(nombre_parametro4, SqlDbType.VarChar).Value = valor_parametro4;
            cmd.CommandType = CommandType.StoredProcedure;
            conexion.Abrir();
            da.SelectCommand = cmd;
            da.Fill(dt);
            conexion.Cerrar();
            return dt;
        }

        public bool existe(string nombre_procedimiento, string nombre_parametro, string valor_parametro)
        {
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand();
            DataTable dt = new DataTable();
            cmd.Connection = conexion.cadena;
            cmd.CommandText = nombre_procedimiento;
            cmd.Parameters.Add(nombre_parametro, SqlDbType.VarChar).Value = valor_parametro;
            cmd.CommandType = CommandType.StoredProcedure;
            conexion.Abrir();
            da.SelectCommand = cmd;
            da.Fill(dt);
            conexion.Cerrar();

            if (dt.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool existe(string nombre_procedimiento, string nombre_parametro1, string valor_parametro1, string nombre_parametro2, int valor_parametro2)
        {
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand();
            DataTable dt = new DataTable();
            cmd.Connection = conexion.cadena;
            cmd.CommandText = nombre_procedimiento;
            cmd.Parameters.Add(nombre_parametro1, SqlDbType.VarChar).Value = valor_parametro1;
            cmd.Parameters.Add(nombre_parametro2, SqlDbType.Int).Value = valor_parametro2;
            cmd.CommandType = CommandType.StoredProcedure;
            conexion.Abrir();
            da.SelectCommand = cmd;
            da.Fill(dt);
            conexion.Cerrar();

            if (dt.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool existe(string nombre_procedimiento, string nombre_parametro1, string valor_parametro1, string nombre_parametro2, string valor_parametro2, string nombre_parametro3, string valor_parametro3, string nombre_parametro4, string valor_parametro4)
        {
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand();
            DataTable dt = new DataTable();
            cmd.Connection = conexion.cadena;
            cmd.CommandText = nombre_procedimiento;
            cmd.Parameters.Add(nombre_parametro1, SqlDbType.VarChar).Value = valor_parametro1;
            cmd.Parameters.Add(nombre_parametro2, SqlDbType.VarChar).Value = valor_parametro2;
            cmd.Parameters.Add(nombre_parametro3, SqlDbType.VarChar).Value = valor_parametro3;
            cmd.Parameters.Add(nombre_parametro4, SqlDbType.VarChar).Value = valor_parametro4;
            cmd.CommandType = CommandType.StoredProcedure;
            conexion.Abrir();
            da.SelectCommand = cmd;
            da.Fill(dt);
            conexion.Cerrar();

            if (dt.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
