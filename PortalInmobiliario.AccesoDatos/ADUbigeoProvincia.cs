﻿using System.Data;
using System.Data.SqlClient;
using PortalInmobiliario.Entidades;

namespace PortalInmobiliario.AccesoDatos
{
    public class ADUbigeoProvincia : Consulta
    {
        public void crear(string nombre_procedimiento, UbigeoProvincia provincia)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexion.cadena;
            cmd.CommandText = nombre_procedimiento;
            cmd.Parameters.Add("@nombre", SqlDbType.VarChar).Value = provincia.Nombre;
            cmd.Parameters.Add("@ubigeo_departamento_id", SqlDbType.Int).Value = provincia.UbigeoDepartamentoId;
            cmd.CommandType = CommandType.StoredProcedure;
            conexion.Abrir();
            cmd.ExecuteNonQuery();
            conexion.Cerrar();
        }
    }
}
