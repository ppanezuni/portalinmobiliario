﻿using System.Data;
using System.Data.SqlClient;
using PortalInmobiliario.Entidades;

namespace PortalInmobiliario.AccesoDatos
{
    public class ADDepartamento : Consulta
    {
        public void crear(string nombre_procedimiento, Departamento departamento_proyecto)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexion.cadena;
            cmd.CommandText = nombre_procedimiento;
            cmd.Parameters.Add("@codigo", SqlDbType.VarChar).Value = departamento_proyecto.Codigo;
            cmd.Parameters.Add("@area_total", SqlDbType.Decimal).Value = departamento_proyecto.AreaTotal;
            cmd.Parameters.Add("@precio_m2", SqlDbType.Decimal).Value = departamento_proyecto.PrecioM2;
            cmd.Parameters.Add("@moneda", SqlDbType.VarChar).Value = departamento_proyecto.Moneda;
            cmd.Parameters.Add("@piso_ubicado", SqlDbType.Int).Value = departamento_proyecto.PisoUbicado;
            cmd.Parameters.Add("@reservado", SqlDbType.Bit).Value = departamento_proyecto.Reservado;
            cmd.Parameters.Add("@proyecto_id", SqlDbType.Int).Value = departamento_proyecto.ProyectoId;
            cmd.CommandType = CommandType.StoredProcedure;
            conexion.Abrir();
            cmd.ExecuteNonQuery();
            conexion.Cerrar();
        }

        public void actualizar(string nombre_procedimiento, Departamento departamento_proyecto)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexion.cadena;
            cmd.CommandText = nombre_procedimiento;
            cmd.Parameters.Add("@id", SqlDbType.Int).Value = departamento_proyecto.Id;
            cmd.Parameters.Add("@codigo", SqlDbType.VarChar).Value = departamento_proyecto.Codigo;
            cmd.Parameters.Add("@area_total", SqlDbType.Decimal).Value = departamento_proyecto.AreaTotal;
            cmd.Parameters.Add("@precio_m2", SqlDbType.Decimal).Value = departamento_proyecto.PrecioM2;
            cmd.Parameters.Add("@moneda", SqlDbType.VarChar).Value = departamento_proyecto.Moneda;
            cmd.Parameters.Add("@piso_ubicado", SqlDbType.Int).Value = departamento_proyecto.PisoUbicado;
            cmd.Parameters.Add("@reservado", SqlDbType.Bit).Value = departamento_proyecto.Reservado;
            cmd.Parameters.Add("@proyecto_id", SqlDbType.Int).Value = departamento_proyecto.ProyectoId;
            cmd.CommandType = CommandType.StoredProcedure;
            conexion.Abrir();
            cmd.ExecuteNonQuery();
            conexion.Cerrar();
        }
    }
}
