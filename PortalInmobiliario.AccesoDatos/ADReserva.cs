﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortalInmobiliario.Entidades;
using System.Data;
using System.Data.SqlClient;

namespace PortalInmobiliario.AccesoDatos
{
    public class ADReserva : Consulta
    {
        public void crear(string nombreProcedimiento, Reserva reservaObj)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexion.cadena;
            cmd.CommandText = nombreProcedimiento;
            cmd.Parameters.Add("@departamento_id", SqlDbType.Int).Value = reservaObj.DepartamentoId;
            cmd.Parameters.Add("@cliente_id", SqlDbType.Int).Value = reservaObj.ClienteId;
            cmd.Parameters.Add("@fecha_reserva", SqlDbType.DateTime).Value = reservaObj.fechaReserva;
            cmd.CommandType = CommandType.StoredProcedure;
            conexion.Abrir();
            cmd.ExecuteNonQuery();
            conexion.Cerrar();
        }
    }
}
