﻿using System.Data;
using System.Data.SqlClient;
using PortalInmobiliario.Entidades;

namespace PortalInmobiliario.AccesoDatos
{
    public class ADAgente : Consulta
    {
        public void crear(string nombre_procedimiento, Agente agente_inmobiliario)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexion.cadena;
            cmd.CommandText = nombre_procedimiento;
            cmd.Parameters.Add("@dni", SqlDbType.VarChar).Value = agente_inmobiliario.Dni;
            cmd.Parameters.Add("@apellido_paterno", SqlDbType.VarChar).Value = agente_inmobiliario.ApellidoPaterno;
            cmd.Parameters.Add("@apellido_materno", SqlDbType.VarChar).Value = agente_inmobiliario.ApellidoMaterno;
            cmd.Parameters.Add("@nombres", SqlDbType.VarChar).Value = agente_inmobiliario.Nombres;
            cmd.Parameters.Add("@telefono", SqlDbType.VarChar).Value = agente_inmobiliario.Telefono;
            cmd.Parameters.Add("@correo_electronico", SqlDbType.VarChar).Value = agente_inmobiliario.CorreoElectronico;
            cmd.CommandType = CommandType.StoredProcedure;
            conexion.Abrir();
            cmd.ExecuteNonQuery();
            conexion.Cerrar();
        }
    }
}
