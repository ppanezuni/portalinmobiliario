﻿using System.Data;
using System.Data.SqlClient;
using PortalInmobiliario.Entidades;

namespace PortalInmobiliario.AccesoDatos
{
    public class ADCliente : Consulta
    {
        public void crear(string nombre_procedimiento, Cliente cliente_registrado)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexion.cadena;
            cmd.CommandText = nombre_procedimiento;
            cmd.Parameters.Add("@dni", SqlDbType.VarChar).Value = cliente_registrado.Dni;
            cmd.Parameters.Add("@apellido_paterno", SqlDbType.VarChar).Value = cliente_registrado.ApellidoPaterno;
            cmd.Parameters.Add("@apellido_materno", SqlDbType.VarChar).Value = cliente_registrado.ApellidoMaterno;
            cmd.Parameters.Add("@nombres", SqlDbType.VarChar).Value = cliente_registrado.Nombres;
            cmd.Parameters.Add("@telefono", SqlDbType.VarChar).Value = cliente_registrado.Telefono;
            cmd.Parameters.Add("@correo_electronico", SqlDbType.VarChar).Value = cliente_registrado.CorreoElectronico;
            cmd.CommandType = CommandType.StoredProcedure;
            conexion.Abrir();
            cmd.ExecuteNonQuery();
            conexion.Cerrar();
        }
    }
}
