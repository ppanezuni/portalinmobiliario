﻿using System.Data;
using System.Data.SqlClient;
using PortalInmobiliario.Entidades;

namespace PortalInmobiliario.AccesoDatos
{
    public class ADUbigeoDepartamento : Consulta
    {
        public void crear(string nombre_procedimiento, UbigeoDepartamento departamento)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexion.cadena;
            cmd.CommandText = nombre_procedimiento;
            cmd.Parameters.Add("@nombre", SqlDbType.VarChar).Value = departamento.Nombre;
            cmd.CommandType = CommandType.StoredProcedure;
            conexion.Abrir();
            cmd.ExecuteNonQuery();
            conexion.Cerrar();
        }
    }
}
