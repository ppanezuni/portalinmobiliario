﻿using System.Data;
using System.Data.SqlClient;
using PortalInmobiliario.Entidades;

namespace PortalInmobiliario.AccesoDatos
{
    public class ADFotoXProyecto : Consulta
    {
        public void crear(string nombre_procedimiento, FotoXProyecto foto_proyecto)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexion.cadena;
            cmd.CommandText = nombre_procedimiento;
            cmd.Parameters.Add("@ruta", SqlDbType.VarChar).Value = foto_proyecto.Ruta;
            cmd.Parameters.Add("@proyecto_id", SqlDbType.Int).Value = foto_proyecto.ProyectoId;
            cmd.CommandType = CommandType.StoredProcedure;
            conexion.Abrir();
            cmd.ExecuteNonQuery();
            conexion.Cerrar();
        }
    }
}
