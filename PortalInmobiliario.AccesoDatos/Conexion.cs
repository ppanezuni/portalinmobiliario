﻿using System.Data.SqlClient;

namespace PortalInmobiliario.AccesoDatos
{
    public class Conexion
    {
        //  Cadena de Conexión usando Autenticación Sql Server
        public SqlConnection cadena = new SqlConnection("Data Source=LAPTOP-65HCKINB\\APICENTROSERVER;Initial Catalog=bd_inmobiliaria;user=sa;password=123456");
        //  Cadena de Conexión usando Autenticación Windows
        //public SqlConnection cadena_conexion = new SqlConnection("Data Source=LAPTOP-65HCKINB\\APICENTROSERVER;Initial Catalog=bd_inmobiliaria;Integrated Security=True");

        public void Abrir()
        {
            cadena.Open();
        }
        public void Cerrar()
        {
            cadena.Close();
        }
    }
}
